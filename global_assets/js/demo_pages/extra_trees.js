/* ------------------------------------------------------------------------------
 *
 *  # Fancytree
 *
 *  Demo JS code for extra_trees.html page
 *
 * ---------------------------------------------------------------------------- */


// Setup module
// ------------------------------

var Fancytree = function() {


    //
    // Setup module components
    //

    // Uniform
    var _componentFancytree = function() {
        if (!$().fancytree) {
            console.warn('Warning - fancytree_all.min.js is not loaded.');
            return;
        }



        //
        // Table tree
        //

        $('.tree-table').fancytree({
            extensions: ['table'],
            checkbox: true,
            table: {
                indentation: 20,      // indent 20px per node level
                nodeColumnIdx: 2,     // render the node title into the 2nd column
                checkboxColumnIdx: 0  // render the checkboxes into the 1st column
            },
            source: {
                url: ASSETS_URL +'global_assets/demo_data/fancytree/fancytree.json'
            },
            lazyLoad: function(event, data) {
                data.result = {url: 'ajax-sub2.json'}
            },
            renderColumns: function(event, data) {
                var node = data.node,
                $tdList = $(node.tr).find('>td');

                // (index #0 is rendered by fancytree by adding the checkbox)
                $tdList.eq(1).text(node.getIndexHier()).addClass('alignRight');

                // (index #2 is rendered by fancytree)
                $tdList.eq(3).text(node.key);
                $tdList.eq(4).addClass('text-center').html('<input type="checkbox" class="form-input-styled" name="like" value="' + node.key + '">');

                // Style checkboxes
                $('.form-input-styled').uniform();
            }
        });

        // Handle custom checkbox clicks
        $('.tree-table').on('input[name=like]', 'click', function(e) {
            var node = $.ui.fancytree.getNode(e),
            $input = $(e.target);
            e.stopPropagation(); // prevent fancytree activate for this row
            if($input.is(':checked')){
                alert('like ' + $input.val());
            }
            else{
                alert('dislike ' + $input.val());
            }
        });
    };


    //
    // Return objects assigned to module
    //

    return {
        init: function() {
            _componentFancytree();
        }
    }
}();


// Initialize module
// ------------------------------

document.addEventListener('DOMContentLoaded', function() {
    Fancytree.init();
});
