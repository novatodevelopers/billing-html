

$(window).scroll(function () {
    var scroll = $(window).scrollTop();
    if (scroll >= 100) {
        $("header").addClass("highlight");
    } else {
        $("header").removeClass("highlight");
    }
});


/* ------------------------------------------------------------------------------
 *
 *  # Steps wizard
 *
 *  Demo JS code for form_wizard.html page
 *
 * ---------------------------------------------------------------------------- */


// Setup module
// ------------------------------

var FormWizard = function () {


    //
    // Setup module components
    //

    // Wizard
    var _componentWizard = function () {
        if (!$().steps) {
            console.warn('Warning - steps.min.js is not loaded.');
            return;
        }

        var form = $('#business_wizard');
        $('.select').select2({
            minimumResultsForSearch: Infinity
        }).trigger('change');
        ;
        $('.form-check-input-styled').uniform();

        // Basic wizard setup
        $('#business_wizard').steps({
            headerTag: 'h3',
            bodyTag: 'fieldset',
            transitionEffect: 'fade',
            titleTemplate: '<span class="number">#index#</span> #title#',
            labels: {
                previous: '<i class="icon-arrow-left13 mr-2" /> Back',
                next: 'Next <i class="icon-arrow-right14 ml-2" />',
                finish: 'Done <i class="icon-arrow-right14 ml-2" />'
            },
            onStepChanging: function (event, currentIndex, newIndex) {

                // Allways allow previous action even if the current form is not valid!
                if (currentIndex > newIndex) {
                    return true;
                }

                // Needed in some cases if the user went back (clean up)
                if (currentIndex < newIndex) {

                    // To remove error styles
                    form.find('.body:eq(' + newIndex + ') label.error').remove();
                    form.find('.body:eq(' + newIndex + ') .error').removeClass('error');
                }

                //console.log(form);
                form.validate().settings.ignore = ':disabled,:hidden';
                return form.valid();
            },
            onFinished: function (event, currentIndex) {
                form.validate().settings.ignore = ':disabled,:hidden';
                var final_form = form.valid();
                if (final_form == true) {
                    $('#business_wizard').submit();
                }

            }
        });

        //
        // Wizard with validation
        //

        // Stop function if validation is missing
        if (!$().validate) {
            console.warn('Warning - validate.min.js is not loaded.');
            return;
        }




        // Initialize validation

        $('#business_wizard').validate({
            //ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
            ignore: '.select2-search__field', // ignore hidden fields
            errorClass: 'validation-invalid-label',
            highlight: function (element, errorClass) {
                $(element).removeClass(errorClass);
            },
            unhighlight: function (element, errorClass) {
                $(element).removeClass(errorClass);
            },
            // Different components require proper error label placement
            errorPlacement: function (error, element) {

                // Unstyled checkboxes, radios
                if (element.parents().hasClass('form-check')) {
                    error.appendTo(element.parents('.form-check').parent());
                }

                // Input with icons and Select2
                else if (element.parents().hasClass('form-group-feedback') || element.hasClass('select2-hidden-accessible')) {
                    error.appendTo(element.parent());
                }

                // Input group, styled file input
                else if (element.parent().is('.uniform-uploader, .uniform-select') || element.parents().hasClass('input-group')) {
                    error.appendTo(element.parent().parent());
                }

                // Other elements
                else {
                    error.insertAfter(element);
                }
            },
            rules: {
                business_name: {
                    required: true
                },
                category: {
                    required: true
                },
                sector: {
                    required: true
                }
            },
            messages: {
                business_name: 'Please input the name of your business',
                "category[]": 'Please select at least one category',
                "sector[]": 'Please select at least one sector',
            }
        });
        
        //$('.sbmt-disable').on('click', function(e){
            //$(this).attr('disable',true);
        //});
    };



    //
    // Return objects assigned to module
    //

    return {
        init: function () {
            _componentWizard();
            //  _componentUniform();
            //  _componentSelect2();
        }
    }
}();


// Initialize module
// ------------------------------

document.addEventListener('DOMContentLoaded', function () {
    FormWizard.init();
});

