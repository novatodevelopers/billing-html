/* ------------------------------------------------------------------------------
 *
 *  # Custom JS code
 *
 *  Place here all your custom js. Make sure it's loaded after app.js
 *
 * ---------------------------------------------------------------------------- */

// for custom icons
const ajax = new XMLHttpRequest();
ajax.open("GET", "global_assets/css/icons/sc-icons-sprite.svg", true);
ajax.send();
ajax.onload = function () {
    const div = document.createElement("div");
    div.className = "svg-sprite";
    div.innerHTML = ajax.responseText;
    document.body.insertBefore(div, document.body.childNodes[0]);
};

jQuery(document).ready(function () {
    //console.log('test');
    jQuery('#filecategory').change(function () {
        jQuery('.hide-content').fadeOut().remove();
    });


    var struct = jQuery('#document_structure').data('uploads');
    var select_box = '';

    jQuery.each(struct, function (i, v) {
        select_box = select_box + '<optgroup label="' + v.name + '">';
        var children = v.children;
        jQuery.each(children, function (j, k) {
            if (k.tree == 'file') {
                select_box = select_box + '<option value="' + k.index + '">' + k.name + '</option>';
            }

            if (k.tree == 'folder') {
                select_box = select_box + '<optgroup label="' + k.name + '">';
                var sub_children = k.children;
                jQuery.each(sub_children, function (x, y) {
                    if (y.tree == 'file') {
                        select_box = select_box + '<option value="' + y.index + '">' + y.name + '</option>';
                    }
                });
                select_box = select_box + '</optgroup>';
            }
        });
        select_box = select_box + '</optgroup>';
    }); //console.log(select_box);
    jQuery('#filecategory').append(select_box);



    jQuery('.select').select2({
        // minimumResultsForSearch: Infinity
    });
    jQuery('.form-check-input-styled').uniform();

    $(".fe-mobile-toggle").on('click', function () {
        $(".fe-mobile-toggle .icon").toggleClass('icon-menu7 icon-cross2');
        $('header').toggleClass('show-menu');
    });

    $(".fe-blog-search-btn").on('click', function () {
        $(".fe-blog-search-btn .icon").toggleClass('icon-search4 icon-cross2');
        $('header').toggleClass('show-search');
    });

    $(".fe-scroll").on('click', function () {
        event.preventDefault();

        $('html, body').animate({
            scrollTop: $($.attr(this, 'href')).offset().top -120
        }, 500);
    });

    // click to load pdf
    $('.sc-show-preview').on('click', function() {

        
        newDoc = $(this).data('url');
        targetPane = $(this).data('target');
        docTitle = $(this).data('doc-title');
        currentFile = $("#" + targetPane + "-url").val();
        
        console.log("newDoc: " + newDoc + " /r currentFile: " + currentFile)
        
        if(newDoc == currentFile) {
            console.log("same...");
            $(this).toggleClass('sc-active');
            togglePDFPreview();
        }else{
            console.log("different...");
            $('body:not(.show-pdf-overlay)').addClass('show-pdf-overlay');
            loadDynamicPdf(newDoc, targetPane, docTitle, false, false);
            $('.sc-active').removeClass('sc-active');
            $(this).addClass('sc-active');
        }

    })


    // click to show tender info
    $('.action-tender-info').on('click', function() {
        
        newDoc = $(this).data('url');
        targetPane = $(this).data('target');
        docTitle = $(this).data('doc-title');
        currentFile = $("#" + targetPane + "-url").val();
        
        closeDocReview();
        closeDocUpload();

        if($(this).hasClass('sc-active')) {
            closeSupportingDoc();
        } else {
            $('body').addClass('show-upload-document-panel');
            $('.action-tender-info').addClass('sc-active');
            $('.sc-application-item.action-tender-info').addClass('sc-active-doc');
            $('#tender-info').addClass('show');
            $('.sc-review-empty-state').addClass('hide');
        }
        
        // if(newDoc != currentFile) {
            loadDynamicPdf(newDoc, targetPane, docTitle, false, true);
        // }

    })


    // click to show note pad
    $('.action-notepad').on('click', function() {
        $('.btn-dock-nav.action-notepad').toggleClass('sc-active');
        $('body').toggleClass('show-notepad');
    })

    // toggle grouped items
    $('body').on('click','.sc-application-item-group-title', function() {
        $(this).parent().toggleClass('show');
        $(this).toggleClass('sc-group-active');
        // closeDocReview();
        // closeDocUpload();
        return false;
    })
    // close review panel
    $('body').on('click','.close-review', function() {
        closeDocReview();
    })
    


    // close upload panel
    $('body').on('click','.close-upload', function() {
        closeDocUpload();
    })
    // click to show upload document
    $('body').on('click','.action-upload-doc', function() {

        docId = $(this).data('id');
        closeDocReview();
        closeDocUpload();
        closeSupportingDoc();
        
        $('.sc-active-doc').removeClass('sc-active-doc');
        $('body').addClass('show-upload-document-panel');
        $(this).addClass('sc-active-doc');
        $('#upload-' + docId).addClass('show');
        $('.sc-review-empty-state').addClass('hide');

    })

    // TIMER COUNTDOWN
    $(".countdown-timer").each(function( index ) {
        // Set the date we're counting down to
        theDate = new Date($(this).data('expiry')).getTime();
        theTargetId = $(this).attr("id");

        timerCountdown(theDate, theTargetId);
    });

    // TODO ITEMS
    // to do notes - add item
    $('.sc-section-add-todo').submit(function() {
        if( $(this).find('.new-todo').val().length > 5) {
            var newTask = $(this).find('.new-todo').val();
            var newLi = $('<label class="sc-todo-row" role="checkbox"><input type="checkbox" role="checkbox" onchange=""><div class="sc-todo-item rounded"><i class="icon-checkmark5 sc-item-checkmark mr-2"></i><span class="sc-todo-title">' + newTask + '</span><button class="btn ml-auto btn-icon todo-delete-item"><i class="icon-minus3"></i></button></div></label>');
            $(this).prev().append(newLi);
            $(this).find('.new-todo').val('');
        } else {
            alert("empty");
        }
        return false;
    })
    
    // to do notes - delete item
    $('body').on('click', '.todo-delete-item', function(){
        $(this).parent().parent().remove();
    });

    // checking box on checklist popup
    $('body').on('click', '.action-add-to-checklist', function(){

        theSection = $(this).data('section');
        theItemLabel = $(this).data('title');
        typeBadge = $(this).data('badge');
        itemId = $(this).attr('id');
        
        console.log('sect:' + theSection + ' label: ' + theItemLabel + ' badge [' + typeBadge + "] id: " + itemId);

        if($(this).prop("checked") == true){
            console.log("Checkbox is checked.");
            addToChecklist("#" + theSection, theItemLabel, typeBadge, itemId);
        }
        else if($(this).prop("checked") == false){
            console.log("Checkbox is unchecked.");
            $('#list-item-' + itemId).remove();
            checklistEmptyStatus();
        }
        
        selectCount = $('.action-add-to-checklist:checked').length;
        $("#add_" + theSection + " .select-counter").html(selectCount);

        console.log("#add_" + theSection + " .select-counter");
    });

    // close review success modal

    $(".close-success").click(function(){
        $(".success").addClass('close-success');
    })

});
// show application progress on mobile
function showApplicationProgress(){
    $('body').toggleClass('show-app-progress');
}
// focus mode
function toggleFocusMode(){
    $('body').toggleClass('focus-mode-active');
}
function focusCheck(){
    if($('#focusMode').prop('checked')) {
        $('body').addClass('focus-mode-active');
    } else {
        $('body').removeClass('focus-mode-active');
    }
}
// separate documents toggle
function separateDocuments(){
    if($('#separate_documents').prop('checked')) {
        $('#section_break_marker').addClass('show');
    } else {
        $('#section_break_marker').removeClass('show');
    }
}

// include tender doc toggle
function includeTenderDoc(){
    if($('#include_tender_doc').prop('checked')) {
        $('#tender_doc_paceholder').addClass('show');
    } else {
        $('#tender_doc_paceholder').removeClass('show');
    }
}


// table of contents toggle
function includeTableOfContents(){
    if($('#add_table_of_contents').prop('checked')) {
        $('#table_of_contents_paceholder').addClass('show');
    } else {
        $('#table_of_contents_paceholder').removeClass('show');
    }
}

// adobe pdf embed
function loadDynamicPdf(newPDF, targetDiv, documentTitle, setAnnotationTools, pageThumbnails){
            
    // clientID for socialightmediakenya.com
    // var adobeDCView = new AdobeDC.View({clientId: "07f4280d9f64418a93fa380bc18f2bdb", divId: targetDiv});
    
    // clientID for localhost
    var adobeDCView = new AdobeDC.View({clientId: "630e10d9f72f4e3daa0162a81c060665", divId: targetDiv});
    adobeDCView.previewFile(
        {
            content:   {location: {url: newPDF}},
            metaData: {fileName: documentTitle}
        },
        {
            showAnnotationTools: setAnnotationTools,
            showLeftHandPanel: pageThumbnails
        });
        
    $("#" + targetDiv + "-url").val(newPDF);

    console.log("#" + targetDiv + ".data('url') set to: " + newPDF);
}

// mobile pdf preview toggle
function togglePDFPreview() {
    $('body').toggleClass('show-pdf-overlay');
}

// add popup list items
function showAddListPopup(targetList){
    $("#" + targetList).addClass('show');
    // autoFocus();
    $('body').addClass('no-scroll');
    $('.sc-proceed-button').prop('disabled', true);
    $('html, body').animate({
        scrollTop: $(".sc-application-list-panel").offset().top - 50
    }, 500);
}
function closeAddListPopup(targetList){
    $("#" + targetList).removeClass('show');
    $('body').removeClass('no-scroll');
    $('.sc-proceed-button').prop('disabled', false);
    $('html, body').delay().animate({
        scrollTop: $(".sc-application-list-panel").offset().top - 50
    }, 500);
}
function addDirector(){
    directorName = $('#director_firstname').val() + " " + $('#director_lastname').val();
    addtoPopup('#company_directors', directorName);
    addToChecklist("#checklist_technical", directorName, "Director");
    $('#modal_add_director').modal('hide');
    $('#add_director_form').trigger('reset');
}
function addOther(sourceModal,targetPopupList,targetList){
    documentTitle = $('#'+sourceModal + " #title").val();
    addtoPopup('#' + targetPopupList, documentTitle);
    addToChecklist("#"+targetList, documentTitle);
    $('#'+sourceModal).modal('hide');
    $('#'+sourceModal + " form").trigger('reset');
}
function addtoPopup(popupSection, popupItemLabel) {
    $(popupSection).append('<label class="sc-checklist-input" role="checkbox"><input type="checkbox" checked><div class="sc-application-checklist-item rounded"><i class="icon-checkmark5 sc-item-checkmark m-1"></i><span class="sc-application-item-title">' + popupItemLabel + '</span></div></label>');
}

// show/hide checklist section empty state
function checklistEmptyStatus(){
    $('.checklist-items').each(function( index ) {
        if($(this).children().length > 0) {
            $(this).next('.sc-section-empty-state').addClass('hide');
        } else {
            $(this).next('.sc-section-empty-state').removeClass('hide');
        }
    });
}

function addToChecklist(checklistSection, checklistItemLabel, typeLabel, checkBoxId) {
    if(typeLabel) {
        labelBadge = ' <span class="badge bg-light text-muted border">' + typeLabel +'</span>';
    } else {
        labelBadge = "";
    }
    $(checklistSection).append('<div id="list-item-' + checkBoxId + '" class="sc-organize-item sc-application-item rounded"><span class="sc-application-item-title">' + checklistItemLabel + labelBadge + '</span><div class="sc-application-item-right d-flex"><button class="btn bg-transparent border ml-auto sc-drag-move pointer-only"><!-- <i class="icon-move-alt1"></i> --></button><button class="btn bg-transparent btn-link ml-1 my-auto sc-touch-move-up touch-only"><i class="icon-arrow-up12"></i></button><button class="btn bg-transparent btn-link ml-1 my-auto sc-touch-move-down touch-only"><i class="icon-arrow-down12"></i></button></div></div>');
    $(checklistSection).next(".sc-section-empty-state").addClass('hide');

    checklistEmptyStatus();
}

function autoFocus() {
    if(!$('body').hasClass('focus-mode-active')) {
        
        $("#focusMode").prop("checked", true);
        if($(window).width() > 991) {
            $('body').addClass('focus-mode-active');
            $.jGrowl('Focus Mode turned on', {
                theme: 'bg-slate',
                sticky: false
            });
        }
    }
}

// autosave
function autoSaveFeedback(saveStatus){
    var dt = new Date();
    var time = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
    if(saveStatus == "ok") {
        $('.sc-save-fail').hide();
        $('.sc-save-ok').fadeIn().html("Saved @ " + time);
        $('.sc-autosave').removeClass('sc-spriteicon-warning').addClass('sc-spriteicon-success');
    } else {
        $('.sc-save-fail').fadeIn();
        $('.sc-save-ok').hide();
        $('.sc-autosave').removeClass('sc-spriteicon-success').addClass('sc-spriteicon-warning');
    }
}
// auto save
var saveTimer = setInterval(autoSave, 30000);
function autoSave() {
    // validation

    // saving

    //success
    autoSaveFeedback('ok');
    
    //fail
    // autoSaveFeedback('error');
}
function abortTimer() {
  clearInterval(saveTimer);
}

// TIMER COUNTDOWN

function timerCountdown(countDownDate, countDownTarget) {
    
    // Update the count down every 1 second
    var x = setInterval(function() {
    
        // Get today's date and time
        var now = new Date().getTime();
    
        // Find the distance between now and the count down date
        var distance = countDownDate - now;
    
        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);
        

        // if more than one day remaining
        if(days > 0) {
            $("#"+countDownTarget).addClass("badge-success");
        } 

        // if less than one day remaining
        if(days < 1) {
            $("#"+countDownTarget)
                .removeClass("badge-success")
                .addClass("badge-warning");
        } 

        // leading zeroes
        hours = pad(hours, 2);
        minutes = pad(minutes, 2);
        seconds = pad(seconds, 2);
    
        // Display the result in the element with id="demo"
        $("#"+countDownTarget).html(
            days + "d " 
            + hours + "h "
            + minutes + "m " 
            + seconds + "s "
        );

        // If the count down is finished, write some text 
        if (distance < 0) {
            clearInterval(x);
            $("#"+countDownTarget)
                .html("Closed")
                .removeClass("badge-warning")
                .addClass("badge-danger");
        }
    }, 1000);
}
// leading zeroes
function pad (str, max) {
    str = str.toString();
    return str.length < max ? pad("0" + str, max) : str;
}

function toggleExportOptions(){
    $('.export-options').toggleClass('hide');
    $('.toggle-export-options').toggleClass('active');
    if( $('.toggle-export-options').hasClass('active')){
        $('.toggle-export-options').attr('data-original-title','Show export options')
    } else {
        $('.toggle-export-options').attr('data-original-title','Hide export options')

    }
}